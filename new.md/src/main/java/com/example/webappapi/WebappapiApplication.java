package com.example.webappapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.example.webappapi.Repository")
@EnableJpaAuditing
@SpringBootApplication
public class WebappapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebappapiApplication.class, args);
	}

}
