package com.example.webappapi.Controller;

import com.example.webappapi.Repository.user_addressRepository;
import com.example.webappapi.Repository.userdataRepository;
import com.example.webappapi.UserData.user_address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class user_addressController {

    @Autowired
    user_addressRepository user_addressRepository;

    @Autowired
    userdataRepository userdataRepository;

    @GetMapping("/userdata/{user_data_id}/user_address")
    public List<user_address> getUserAddress(@PathVariable(value = "id") Integer user_data_id){
        return user_addressRepository.findByUser_data_id(user_data_id);
    }

    @PostMapping("/userdata/{user_data_id}/user_address")
    public user_address createUserAddress(@PathVariable(value = "userId") Integer user_data_id,
                                          @Valid @RequestBody user_address user_address) throws ResourceNotFoundException{
        return userdataRepository.findById(user_data_id).map(userdata -> {
            user_address.setUserdata(userdata);
            return user_addressRepository.save(user_address);
        }).orElseThrow(() -> new org.springframework.data.rest.webmvc.ResourceNotFoundException("user data not found"));
    }

}
