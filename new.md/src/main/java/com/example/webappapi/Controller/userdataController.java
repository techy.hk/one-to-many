package com.example.webappapi.Controller;

import com.example.webappapi.Repository.userdataRepository;
import com.example.webappapi.UserData.userdata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class userdataController {
    @Autowired
    private userdataRepository userdataRepository;


    @GetMapping("/alluserdata")
    public List<userdata> findAllUserData(){

        return userdataRepository.findAll();
    }

    @GetMapping("/userdata/{id}")
    public ResponseEntity<userdata> getUserDataById(@PathVariable(value = "id") Integer id) throws
            ResourceNotFoundException {

        userdata user = userdataRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User Id not found" + id));
        return ResponseEntity.ok().body(user);
    }

    @PostMapping("/userdata")
    public userdata createUser(@Valid @RequestBody userdata userdata){

        return userdataRepository.save(userdata);
    }

}
