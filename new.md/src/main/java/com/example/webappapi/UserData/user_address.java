package com.example.webappapi.UserData;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Data
@AllArgsConstructor
@ToString
@Entity
@Table(name = "user_address")
public class user_address implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer user_address_id;
    private String user_address_city;
    private Integer user_address_zipcode;


    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name = "user_data_id", referencedColumnName = "id")
    private userdata userdata;

    public user_address(){

    }

    public Integer getUser_address_id() {
        return user_address_id;
    }

    public void setUser_address_id(Integer user_address_id) {
        this.user_address_id = user_address_id;
    }

    public String getUser_address_city() {
        return user_address_city;
    }

    public void setUser_address_city(String user_address_city) {
        this.user_address_city = user_address_city;
    }

    public Integer getUser_address_zipcode() {
        return user_address_zipcode;
    }

    public void setUser_address_zipcode(Integer user_address_zipcode) {
        this.user_address_zipcode = user_address_zipcode;
    }

    public com.example.webappapi.UserData.userdata getUserdata() {
        return userdata;
    }

    public void setUserdata(com.example.webappapi.UserData.userdata userdata) {
        this.userdata = userdata;
    }

    @Override
    public String toString() {
        return "user_address{" +
                "user_address_id=" + user_address_id +
                ", user_address_city='" + user_address_city + '\'' +
                ", user_address_zipcode=" + user_address_zipcode +
                ", userdata=" + userdata +
                '}';
    }
}
