package com.example.webappapi.UserData;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

@Data
@AllArgsConstructor
@ToString
@Entity
@Table(name = "userdata")
public class userdata implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String first_Name;
    private String last_Name;


    @OneToMany(mappedBy = "userdata", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    @JoinColumn(name = "user_data_id",referencedColumnName = "id")
    private List<user_address> user_address;

    public userdata(){

    }

    public userdata(String first_Name, String last_Name) {
        this.first_Name = first_Name;
        this.last_Name = last_Name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirst_Name() {
        return first_Name;
    }

    public void setFirst_Name(String first_Name) {
        this.first_Name = first_Name;
    }

    public String getLast_Name() {
        return last_Name;
    }

    public void setLast_Name(String last_Name) {
        this.last_Name = last_Name;
    }

    public List<com.example.webappapi.UserData.user_address> getUser_address() {
        return user_address;
    }

    public void setUser_address(List<com.example.webappapi.UserData.user_address> user_address) {
        this.user_address = user_address;
    }

        @Override
    public String toString() {
        return "userdata{" +
                "id=" + id +
                ", first_Name='" + first_Name + '\'' +
                ", last_Name='" + last_Name + '\'' +
                ", user_address=" + user_address +
                '}';
    }
}
