package com.example.webappapi.Repository;

import com.example.webappapi.UserData.user_address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface user_addressRepository extends JpaRepository<user_address, Integer> {

    default List<user_address> findByUser_data_id(Integer user_data_id) {
        return null;
    }
//    Optional<user_address> findByIdAndUserId(Integer id, Integer user_data_id);

}
