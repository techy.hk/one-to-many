package com.example.webappapi.Repository;

import com.example.webappapi.UserData.userdata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface userdataRepository extends JpaRepository<userdata, Integer> {

}
